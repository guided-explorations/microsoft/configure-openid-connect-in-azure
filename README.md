# Configure OpenID Connect (OIDC) between Azure and GitLab


## Use-cases
* Retrieve temporary credentials from Azure to access cloud services
* Scope role to branch or project

For additional details, see [documentation here](https://docs.microsoft.com/cli/azure/reference-index?view=azure-cli-latest#az-login)


## Steps


## Resources

- https://cloud.google.com/iam/docs/configuring-workload-identity-federation#oidc
